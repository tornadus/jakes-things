#!/bin/bash

# Download Player.exe
mkdir client
cd client

wget https://brkcdn.com/client/Player.exe

cd ..

# Register brickhill.legacy protocol
desktop_file="[Desktop Entry]
Name=Brick Hill
Exec=${PWD}/launcher.sh %u
Type=Application
Terminal=false
MimeType=x-scheme-handler/brickhill.legacy;"

echo "$desktop_file" > brick-hill-legacy.desktop

mv brick-hill-legacy.desktop ~/.local/share/applications

xdg-mime default brick-hill-legacy.desktop x-scheme-handler/brickhill.legacy

# Create launcher
touch launcher.sh
chmod +x ./launcher.sh

client_path="${PWD}/client/Player.exe"
echo "#!/bin/bash" >> launcher.sh
echo 'launch_options="${1##brickhill.legacy://client/}"' >> launcher.sh
echo "client_path='$client_path'" >> launcher.sh
echo 'wine $client_path $launch_options' >> launcher.sh

echo "Finished installation."
